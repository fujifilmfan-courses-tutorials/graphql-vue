## Introduction
This project is built from two tutorials by Jscrambler (2020-10-13):

* [Build a GraphQL API with Node](https://blog.jscrambler.com/build-a-graphql-api-with-node/) ([Source code](https://github.com/JscramblerBlog/node-express-graphql-api))
* [Building a CRUD App with Vue and GraphQL](https://blog.jscrambler.com/building-a-crud-app-with-vue-and-graphql/) ([Source code](https://github.com/JscramblerBlog/vue-graphql-demo/tree/master/src))

## Running the server
Start the server by running `node index.js` from within the `node-graphql-demo/` directory. Running it from the root directory like `node node-graphql-demo/index.js` will cause the database (`my.db`) to be written in the root directory rather than using the existing one in `node-graphql-demo/`.

Test out the API by navigating to `http://localhost:4000/graphql`; an instance of GraphiQL should be running.  You can run some sample mutations and queries.

### Create a contact
---
```
mutation {
    createContact(firstName: "Jon", lastName: "Snow", email: "jonsnow@thenightswatch.com") {
        id,
        firstName,
        lastName,
        email
    }
}
```

Response:
```json
{
  "data": {
    "createContact": {
      "id": "1",
      "firstName": "Jon",
      "lastName": "Snow",
      "email": "jonsnow@thenightswatch.com"
    }
  }
}
```

### Update a contact
---
```
mutation {
    updateContact(id: 1, firstName: "Aegon", lastName: "Targaryen", email: "aegontargaryen@ironthrone.com")
}
```

Response:
```json
{
  "data": {
    "updateContact": "Contact #1 updated"
  }
}
```

### Query all contacts
---
```
query {
    contacts {
        id
        firstName
        lastName
        email
    }
}
```

Response:
```json
{
  "data": {
    "contacts": [
      {
        "id": "1",
        "firstName": "Aegon",
        "lastName": "Targaryen",
        "email": "aegontargaryen@ironthrone.com"
      }
    ]
  }
}
```

### Query one contact
---
```
query {
    contact(id: 1) {
        id
        firstName
        lastName
        email
    }
}
```

Response:
```json
{
  "data": {
    "contact": {
      "id": "1",
      "firstName": "Aegon",
      "lastName": "Targaryen",
      "email": "aegontargaryen@ironthrone.com"
    }
  }
}
```

### Delete a contact
---
```
mutation {
    deleteContact(id: 1)
}
```

Response:
```json
{
  "data": {
    "deleteContact": "Contact #1 deleted"
  }
}
```

After querying contacts again, we get:
```json
{
  "data": {
    "contacts": []
  }
}
```

## Run the Vue app
Start the Vue app by running `npm run serve` from within the `vue-graphql-demo` directory.

Vue tells us that after creating the project with `vue create vue-graphql-demo`:
```bash
🎉  Successfully created project vue-graphql-demo.
👉  Get started with the following commands:

 $ cd vue-graphql-demo
 $ npm run serve
 ```

The app will probably be running at:  
- Local:   http://localhost:8080/  
- Network: http://192.168.0.143:8080/  
