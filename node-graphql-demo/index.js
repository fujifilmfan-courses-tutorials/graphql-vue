const express = require("express");
const sqlite3 = require("sqlite3").verbose();
const graphql = require("graphql");
/*
 Note: I had to change this from `ExpressGraphQL` in the tutorial to `graphqlHTTP`. 
 The former would work if I also used it in `app.use()` as `ExpressGraphQL.graphqlHTTP`.
 https://github.com/graphql/express-graphql/issues/667
*/
const { graphqlHTTP } = require("express-graphql");
const cors = require('cors');

const app = express();
app.use(cors());

const database = new sqlite3.Database("./my.db");

// Create a contacts table in the SQLite3 database and run immediately
const createContactTable = () => {
  const query = `
    CREATE TABLE IF NOT EXISTS contacts (
      id integer PRIMARY KEY,
      firstName text,
      lastName text,
      email text UNIQUE)`;
  return database.run(query);
}
createContactTable();

// Define a GraphQL type, a custom type that corresponds to a contact in the DB
const ContactType = new graphql.GraphQLObjectType({
  name: "Contact",
  fields: {
    id: { type: graphql.GraphQLID },
    firstName: { type: graphql.GraphQLString },
    lastName: { type: graphql.GraphQLString },
    email: { type: graphql.GraphQLString }
  }
});

// Define the GraphQL query type
let queryType = new graphql.GraphQLObjectType({
  name: "Query",
  fields: {
    contacts: {
      type: graphql.GraphQLList(ContactType),
      resolve: (root, args, context, info) => {
        return new Promise((resolve, reject) => {

          // Use `.all()` when we intend to load all results into memory (otherwise, consider `.each()`)
          database.all("SELECT * FROM contacts;", function (err, rows) {
            if (err) {
              reject([]);
            }
            resolve(rows);
          });
        });

      }
    },
    contact: {
      type: ContactType,
      args: {
        id: {
          type: new graphql.GraphQLNonNull(graphql.GraphQLID)
        }
      },
      // Note that we're using { id } for "args" here (defined above)
      resolve: (root, { id }, context, info) => {
        return new Promise((resolve, reject) => {

          database.all("SELECT * FROM contacts WHERE id = (?);", [id], function (err, rows) {
            if (err) {
              reject(null);
            }
            resolve(rows[0]);
          });
        });

      }
    }
  }
});

// Define the GraphQL mutation type
let mutationType = new graphql.GraphQLObjectType({
  name: "Mutation",
  fields: {
    createContact: {
      type: ContactType,
      args: {
        firstName: {
          type: new graphql.GraphQLNonNull(graphql.GraphQLString)
        },
        lastName: {
          type: new graphql.GraphQLNonNull(graphql.GraphQLString)
        },
        email: {
          type: new graphql.GraphQLNonNull(graphql.GraphQLString)
        }
      },
      resolve: (root, {
        firstName,
        lastName,
        email
      }) => {
        return new Promise((resolve, reject) => {
          database.run("INSERT INTO contacts (firstName, lastName, email) VALUES (?,?,?);", 
            [firstName, lastName, email], (err) => {
              if (err) {
                reject(null);
              }
              database.get("SELECT last_insert_rowid() as id", (err, row) => {
                resolve({
                  id: row["id"],
                  firstName: firstName,
                  lastName: lastName,
                  email: email
                });
              });
            });
        })
      }
    },
    updateContact: {
      type: graphql.GraphQLString,
      args: {
        id: {
          type: new graphql.GraphQLNonNull(graphql.GraphQLID)
        },
        firstName: {
          type: new graphql.GraphQLNonNull(graphql.GraphQLString)
        },
        lastName: {
          type: new graphql.GraphQLNonNull(graphql.GraphQLString)
        },
        email: {
          type: new graphql.GraphQLNonNull(graphql.GraphQLString)
        }
      },
      resolve: (root, {
        id,
        firstName,
        lastName,
        email
      }) => {
        return new Promise((resolve, reject) => {
          database.run("UPDATE contacts SET firstName = (?), lastName = (?), email = (?) WHERE id = (?);",
            [firstName, lastName, email, id], (err) => {
              if (err) {
                reject(err);
              }
              resolve(`Contact #${id} updated`);
            });
        })
      }
    },
    deleteContact: {
      type: graphql.GraphQLString,
      args: {
        id: {
          type: new graphql.GraphQLNonNull(graphql.GraphQLID)
        }
      },
      resolve: (root, {
        id
      }) => {
        return new Promise((resolve, reject) => {
          database.run("DELETE from contacts WHERE id = (?);", [id], (err) => {
            if (err) {
              reject(err);
            }
            resolve(`Contact #${id} deleted`);
          });
        })
      }
    }
  }
});

// Create a GraphQL schema
const schema = new graphql.GraphQLSchema({
  query: queryType,
  mutation: mutationType
});

// Mount the `/graphql` endpoint
app.get('/', function(req, res) {
  res.redirect(301, '/graphql')
});
app.use("/graphql", graphqlHTTP({ schema: schema, graphiql: true}));
app.listen(4000, () => {
    console.log("GraphQL server running at http://localhost:4000.");
});
